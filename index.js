const express = require("express");
const bodyParser = require("body-parser");
const axios = require("axios").default;
const cors = require("cors");
const FormData = require("form-data");
const parseDOMString = require("./parseDOMString");
const pool = require("./db");

const app = express();

const port = 5000;

app.use(
  cors({
    origin: "http://localhost:3000",
  })
);

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.post("/fetchSuggestions", (req, res) => {
  if (!req.body.searchTerm) {
    res.send(JSON.stringify({ flag: 0, result: "Invalid request body." }));
    return;
  }
  let searchData = new FormData();
  searchData.append("filter", "company");
  searchData.append("search", req.body.searchTerm);
  axios
    .post("https://www.zaubacorp.com/custom-search", searchData)
    .then((response) => {
      let parsedData = parseDOMString(response.data);
      res.send(JSON.stringify({ flag: 1, suggestions: parsedData }));
    });
});

app.post("/addNewCompany", async (req, res) => {
  if (!req.body) {
    res.send({ flag: 0, result: "Invalid request body." });
    return;
  }
  try {
    const newData = await pool.query(
      "INSERT INTO company_data (company_name, company_CIN) values ($1, $2) RETURNING *",
      [req.body.companyName, req.body.CIN]
    );
    console.log(newData);
    res.send(JSON.stringify({ flag: 1, result: "Operation successful." }));
  } catch (error) {
    console.log(error);
    res.send(JSON.stringify({ flag: 0, result: "Some error occured." }));
  }
});

app.get("/getAllCompanies", async (req, res) => {
  try {
    const allData = await pool.query("SELECT * FROM company_data");
    console.log(allData.rows);
    res.send(JSON.stringify({ flag: 1, result: allData.rows }));
  } catch (error) {
    console.error(error);
    res.send(JSON.stringify({ flag: 0, result: "Some error occured." }));
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
