const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const parseDOMString = (str) => {
  let tempDom = new JSDOM(str);
  let divLists = tempDom.window.document.querySelectorAll(".show");
  let extractedData = [];
  for (let i = 0; i < divLists.length; i++) {
    extractedData.push({
      CIN: divLists[i].id.split("/")[2].trim(),
      label: divLists[i].innerHTML.trim(),
    });
  }
  return extractedData;
};

module.exports = parseDOMString;
