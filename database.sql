CREATE DATABASE me_db;

CREATE table company_data(
    company_id SERIAL PRIMARY KEY,
    company_name VARCHAR (225),
    company_CIN VARCHAR (225)
);